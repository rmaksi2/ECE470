## Checkpoint 1 (due 3/5/18) [link to video](https://youtu.be/TBQfd0o0EM0):
### Files in this directory:
* Checkpoint1.mp4 - A video of the demo
* Scene_Checkpoint1.ttt - a scene file for V-REP
* runCode.py - API code for V-REP

### Installing the simulator for Linux (Mint)
Download V-REP PRO EDU from the Coppelia Robotics [website](http://www.coppeliarobotics.com).  
Once you download the Linux tar file, in terminal at V-Rep directory type: './vrep.sh'

### Python remote API
Linux already comes with Python. Create a folder for your API code (ex. vrep_code).   
Copy following files into the folder:  
*vrep/programming/remoteApiBindings/python/python/vrep.py  
vrep/programming/remoteApiBindings/python/python/vrepConst.py  
vrep/programming/remoteApiBindings/lib/lib/remoteApi.dylib*  
Create a file in the same folder where you'll store the API code. In our case, the file is  
named *test.py*. To run the code, type in terminal *python test.py*

### Inside V-Rep
In Model browser, go to robots->non-mobile. From the list, pick and drag UR3 to the scene.  
Repeat. From Model browser, go to components->grippers, pick and drag Jaco hand.  
Click on Jaco hand, while holding CTRL key, select the red origin of the gripper on UR3.  
Click Assemble (9th icon from the left on the bar). The hand should now be attached  
to the gripper. Repeat for Mico hand.
In order to add a cup to the scene, at Model browser go to household, pick and drag the cup.  

Now we are ready to code the API.

### test.py code overview
Go [here](https://gitlab.engr.illinois.edu/rmaksi2/ECE470/blob/master/FinalProject/vrep_code/test.py) to see the code. We start by importing the necessary modules.  
On line 6, all the open connections are closed, just in case. Starting on line 9, the  
connection to remote API server is established.  
Starting on line 16, we get the handles for all the joints. Since we added two UR3 robots  
it is very important to distinguish between the joints of each. To get the name of the joint  
simply go Scene hierarchy in V-Rep and click the box for one of the UR3's in order to  
show the joint names. After we get all the handles, let's start the simulation (line 65)!  
It's time to get the values for joint variables starting on line 73. Again, make sure  
to distinguish between the joints. On line 134 we start moving the joints. It should  
be clear enough to see 'how much' we move each joint in the code.  Stating on line 154,  
the hands are set to wave at the user! This piece of code is explained in the next section.  
After we perform all the moves, the code prints the current position values of the joints  
starting on line 172. We close the simulation and terminate the connection after all is done.  

### Child Scripts for Hand grippers
For this checkpoint, the hand grippers are moved by using a [Child Script](http://www.coppeliarobotics.com/helpFiles/en/childScripts.htm). There are some  
changes that needs to be performed before we can use them in 'test.py'. First, in Scene  
hierarchy, you should see a JacoHand under the first UR3. Next to it, there should be a  
piece of paper icon that needs to be double clicked. This will open the code of the child script  
for that particular JacoHand. Comment out line 19 by inserting '--' in front of it.  
Starting immediately after commented text, insert:
~~~~
sig=sim.getStringSignal('jacoHand')
if sig~=nil then

sim.clearStringSignal('jacoHand')
if sig=='true' then closing=true else closing=false end
end
~~~~
Check the reference [here](http://www.forum.coppeliarobotics.com/viewtopic.php?f=9&t=1891).
Repeat for MacoHand. Do not forget to change 'jacoHand' to 'macoHand'!  
From now on, we can use out 'test.py' code to control the hands. Starting on line 154,  
hands open and close synchronously two times. Then there is a pause (for video purposes).
