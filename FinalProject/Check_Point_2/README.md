## Checkpoint 2 (due 3/12/18)

### Forward Kinematics of UR3
We implemented the basic forward kinematics of UR3 with V-REP at this checkpoint.

First, we find the initial position and orientation of UR3 with respect to world frame.
Then we find joint angles and implemented the matrix exponentials after which we
calculated the final pose.

Using the matrix logarithm of rotational matrix of the final pose, we found the angular
velocity. With the angular velocity we can find a and theta, which we then use to find
the quaternions.

We referenced this Piazza post by Professor Bretl to find the equations needed:
https://piazza.com/class/jchxn1s6tkg20r?cid=257

We also displayed the dummy objects in the form reference frames as indication of successful
implementation of the forward kinematics. 
