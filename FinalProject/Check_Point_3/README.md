## Checkpoint 3 (due 3/27/18)

### Inverse Kinematics of UR3
Given user input we could create a goal pose, and then determine the joint angles that move the robot's end-effector 
to the desired goal pose by using inverse kinematics.

Firstly, make sure you have Remote API file and related python file in the same folder as Checkpoint3.py.

Then it will asked the user to input 6 parameters, which are intended x, y, z position of goal pose and
rotation angles in degrees around x, y , z axis, which we call a,b,c.

Secondly, we set up an dummy object which indicates the supposed end position that UR3 should move.
After enter we call the inverseKinematics Function to perform the valid joint angle calculation, the
initial guess of joint angles are randomly generalized.

We ask for user input until 3 valid positions are given, and then the simulation will stop.

The way we interfaced with V-Rep's API was the same as for Checkpoint 2, so if you're interested about how we did that
check that section.
### Inverse Kinematics of UR3
To implement the inverse kinematics for the UR3, we need the intial pose M, and an array of the 6 screw matracies.
After that first thing we do in the loop is call the simxGetJointPosition() function to get the update our theta values to
the current theta values in the simulator. 

Then we go into the looping algorithm to find theta values. 

The reference we use for this algorithm is the one discussed in Lecture 15 of the class:
~~~~
T_1in0 = getT_1in0(M, S, theta)
screwMat = logm(np.dot(T_2in0, np.linalg.inv(T_1in0)))
twist = invScrewBrac(screwMat)

counter = 0
final_pose_flag = 1

while (np.linalg.norm(twist) > 0.01):

    if(counter >= 100):
        print("Desired Pose not reachable")
        final_pose_flag = 0
        break

    J = getSpacialJacobian(S, theta)
    thetadot = np.dot(np.linalg.inv(np.dot(np.transpose(J), J) + 0.1*np.identity(6)),
    np.dot(np.transpose(J), twist)) - np.dot(np.identity(6) - np.dot(np.linalg.pinv(J), J), theta)
    theta = theta + (thetadot * k)

    T_1in0 = getT_1in0(M, S, theta)
    screwMat = logm(np.dot(T_2in0, np.linalg.inv(T_1in0)))
    twist = invScrewBrac(screwMat)

    counter = counter + 1
~~~~

