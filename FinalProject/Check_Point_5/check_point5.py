import vrep
import time
import numpy as np
import math
from numpy.linalg import multi_dot, norm, inv
from scipy.linalg import expm, logm
from ece470_lib import *
from usefulFunctions import *
import transforms3d

'''
https://matthew-brett.github.io/transforms3d/reference/transforms3d.euler.html#transforms3d.euler.euler2mat
pip install transforms3d
'''

# Close all open connections (just in case)
vrep.simxFinish(-1)

# Connect to V-REP (raise exception on failure)
clientID = vrep.simxStart('127.0.0.1', 19997, True, True, 5000, 5)
if clientID == -1:
    raise Exception('Failed connecting to remote API server')

# Dummies for ConcretBlock1
result, dummy_0 = vrep.simxCreateDummy(clientID, 0.1, None, vrep.simx_opmode_blocking)
result, dummy_2 = vrep.simxCreateDummy(clientID, 0.1, None, vrep.simx_opmode_oneshot_wait)
result, dummy_3 = vrep.simxCreateDummy(clientID, 0.1, None, vrep.simx_opmode_oneshot_wait)
result, dummy_1 = vrep.simxCreateDummy(clientID, 0.1, None, vrep.simx_opmode_oneshot_wait)
result, dummy_4 = vrep.simxCreateDummy(clientID, 0.1, None, vrep.simx_opmode_oneshot_wait)
result, dummy_5 = vrep.simxCreateDummy(clientID, 0.1, None, vrep.simx_opmode_oneshot_wait)

# Dummies for ConcretBlock2
result, dummy_6 = vrep.simxCreateDummy(clientID, 0.1, None, vrep.simx_opmode_blocking)
result, dummy_7 = vrep.simxCreateDummy(clientID, 0.1, None, vrep.simx_opmode_oneshot_wait)
result, dummy_8 = vrep.simxCreateDummy(clientID, 0.1, None, vrep.simx_opmode_oneshot_wait)
result, dummy_9 = vrep.simxCreateDummy(clientID, 0.1, None, vrep.simx_opmode_oneshot_wait)
result, dummy_10 = vrep.simxCreateDummy(clientID, 0.1, None, vrep.simx_opmode_oneshot_wait)
result, dummy_11 = vrep.simxCreateDummy(clientID, 0.1, None, vrep.simx_opmode_oneshot_wait)

# Get handle for the ConcretBlock1
result, ConcretBlock1_handle = vrep.simxGetObjectHandle(clientID, 'ConcretBlock1', vrep.simx_opmode_blocking)
if result != vrep.simx_return_ok:
    raise Exception('could not get object handle for the ConcretBlock1')

# Get handle for the ConcretBlock2
result, ConcretBlock2_handle = vrep.simxGetObjectHandle(clientID, 'ConcretBlock2', vrep.simx_opmode_blocking)
if result != vrep.simx_return_ok:
    raise Exception('could not get object handle for the ConcretBlock2')

# Get position of the ConcretBlock1
result, ConcretBlock1_position = vrep.simxGetObjectPosition(clientID, ConcretBlock1_handle, -1, vrep.simx_opmode_blocking)
if result != vrep.simx_return_ok:
    raise Exception('Could not get object position for the ConcretBlock1')
ConcretBlock1_position = np.reshape(ConcretBlock1_position,(3,1))

# Get position of the ConcretBlock2
result, ConcretBlock2_position = vrep.simxGetObjectPosition(clientID, ConcretBlock2_handle, -1, vrep.simx_opmode_blocking)
if result != vrep.simx_return_ok:
    raise Exception('Could not get object position for the ConcretBlock2')
ConcretBlock2_position = np.reshape(ConcretBlock2_position,(3,1))

# Draw the dummies for the ConcretBlock1
vrep.simxSetObjectPosition(clientID, dummy_0, -1, ConcretBlock1_position, vrep.simx_opmode_oneshot_wait)
vrep.simxSetObjectPosition(clientID, dummy_1, dummy_0, [0,0,-0.1], vrep.simx_opmode_oneshot_wait)
vrep.simxSetObjectPosition(clientID, dummy_2, dummy_0, [0,0,-0.2], vrep.simx_opmode_oneshot_wait)
vrep.simxSetObjectPosition(clientID, dummy_3, dummy_0, [0,0,-0.3], vrep.simx_opmode_oneshot_wait)
vrep.simxSetObjectPosition(clientID, dummy_4, dummy_0, [0,0,0.1], vrep.simx_opmode_oneshot_wait)
vrep.simxSetObjectPosition(clientID, dummy_5, dummy_0, [0,0,0.2], vrep.simx_opmode_oneshot_wait)

# Draw the dummies for the ConcretBlock2
vrep.simxSetObjectPosition(clientID, dummy_6, -1, ConcretBlock2_position, vrep.simx_opmode_oneshot_wait)
vrep.simxSetObjectPosition(clientID, dummy_7, dummy_6, [0,0,-0.1], vrep.simx_opmode_oneshot_wait)
vrep.simxSetObjectPosition(clientID, dummy_8, dummy_6, [0,0,-0.2], vrep.simx_opmode_oneshot_wait)
vrep.simxSetObjectPosition(clientID, dummy_9, dummy_6, [0,0,-0.3], vrep.simx_opmode_oneshot_wait)
vrep.simxSetObjectPosition(clientID, dummy_10, dummy_6, [0,0,0.1], vrep.simx_opmode_oneshot_wait)
vrep.simxSetObjectPosition(clientID, dummy_11, dummy_6, [0,0,0.2], vrep.simx_opmode_oneshot_wait)

# Create p_obstacle array for collision checking and path planner

# Lists for final p_obstacle
entry_one = list()
entry_two = list()
entry_three = list()

# First, we handle first dummy (string combination), then we handle the rest in a loop
result, dummy_handle = vrep.simxGetObjectHandle(clientID, 'Dummy', vrep.simx_opmode_oneshot_wait)
if result != vrep.simx_return_ok:
    raise Exception('could not get object handle for Dummy')

result, dummy_position = vrep.simxGetObjectPosition(clientID, dummy_handle, -1, vrep.simx_opmode_oneshot_wait)
if result != vrep.simx_return_ok:
    raise Exception('Could not get object position for Dummy')
entry_one.append(dummy_position[0])
entry_two.append(dummy_position[1])
entry_three.append(dummy_position[2])

# Loop for the rest
for i in range(11):
    dummy_handle_name = 'Dummy' + str(i)
    result, dummy_handle = vrep.simxGetObjectHandle(clientID, dummy_handle_name, vrep.simx_opmode_blocking)
    if result != vrep.simx_return_ok:
        raise Exception('could not get object handle for ', dummy_handle_name)

    dummy_name = 'dummy_' + str(i+1)
    result, dummy_position = vrep.simxGetObjectPosition(clientID, dummy_handle, -1, vrep.simx_opmode_blocking)
    if result != vrep.simx_return_ok:
        raise Exception('Could not get object position for ', dummy_name)
    entry_one.append(dummy_position[0])
    entry_two.append(dummy_position[1])
    entry_three.append(dummy_position[2])
entry_one = np.asarray(entry_one)
entry_two = np.asarray(entry_two)
entry_three = np.asarray(entry_three)

#################### p_obstacle ################################################
p_obstacle = np.block([
[np.asarray(entry_one)], [np.asarray(entry_two)], [np.asarray(entry_three)]
])

# Get joint object handles
result, joint_one_handle = vrep.simxGetObjectHandle(clientID, 'UR3_joint1', vrep.simx_opmode_blocking)
if result != vrep.simx_return_ok:
    raise Exception('could not get object handle for first joint')

result, joint_two_handle = vrep.simxGetObjectHandle(clientID, 'UR3_joint2', vrep.simx_opmode_blocking)
if result != vrep.simx_return_ok:
    raise Exception('could not get object handle for second joint')

result, joint_three_handle = vrep.simxGetObjectHandle(clientID, 'UR3_joint3', vrep.simx_opmode_blocking)
if result != vrep.simx_return_ok:
    raise Exception('could not get object handle for third joint')

result, joint_four_handle = vrep.simxGetObjectHandle(clientID, 'UR3_joint4', vrep.simx_opmode_blocking)
if result != vrep.simx_return_ok:
    raise Exception('could not get object handle for fourth joint')

result, joint_five_handle = vrep.simxGetObjectHandle(clientID, 'UR3_joint5', vrep.simx_opmode_blocking)
if result != vrep.simx_return_ok:
    raise Exception('could not get object handle for fifth joint')

result, joint_six_handle = vrep.simxGetObjectHandle(clientID, 'UR3_joint6', vrep.simx_opmode_blocking)
if result != vrep.simx_return_ok:
    raise Exception('could not get object handle for sixth joint')

#Get handel for UR3
result, UR3_handle = vrep.simxGetObjectHandle(clientID, 'UR3', vrep.simx_opmode_blocking)
if result != vrep.simx_return_ok:
    raise Exception('could not get object handle for UR3')
#Get handel for UR3_connection
result, UR3_connection = vrep.simxGetObjectHandle(clientID, 'UR3_connection', vrep.simx_opmode_blocking)
if result != vrep.simx_return_ok:
    raise Exception('could not get object handle for UR3UR3_connection')


#Start simulation
vrep.simxStartSimulation(clientID, vrep.simx_opmode_oneshot)

# Get the orientation from base to  world frame
result, orientation = vrep.simxGetObjectOrientation(clientID, UR3_connection, UR3_handle, vrep.simx_opmode_blocking)
if result != vrep.simx_return_ok:
    raise Exception('could not get object orientation angles for UR3')

# Get the position from base to world frame
result, p = vrep.simxGetObjectPosition(clientID, UR3_connection, UR3_handle, vrep.simx_opmode_blocking)
if result != vrep.simx_return_ok:
    raise Exception('could not get object current position for UR3')
P_initial = np.reshape(p,(3,1))

# Return matrix for rotations around z, y and x axes
R_initial = transforms3d.euler.euler2mat(orientation[0], orientation[1], orientation[2])
M = np.array([[0,0,-1, P_initial[0][0]], [0,1,0, P_initial[1][0]], [1,0,0, P_initial[2][0]], [0,0,0,1]])

a1 = np.array([[0],[0],[1]])
q1 = np.array([[0], [0], [0.1045]])
S1 = toScrew(a1, q1)

a2 = np.array([[-1],[0],[0]])
q2 = np.array([[-0.1115], [0], [0.1089]])
S2 = toScrew(a2,q2)

a3 = np.array([[-1],[0],[0]])
q3 = np.array([[-0.1115], [0], [0.3525]])
S3 = toScrew(a3,q3)

a4 = np.array([[-1],[0],[0]])
q4 = np.array([[-0.1115], [0], [0.5658]])
S4 = toScrew(a4,q4)

a5 = np.array([[0],[0],[1]])
q5 = np.array([[-0.1122], [0], [0.65]])
S5 = toScrew(a5,q5)

a6 = np.array([[-1],[0],[0]])
q6 = np.array([[0.1115], [0], [0.6511]])
S6 = toScrew(a6,q6)

S = np.block([[S1, S2, S3, S4, S5, S6]])


############### Get the values for joint variables #############################

result1, theta1 = vrep.simxGetJointPosition(clientID, joint_one_handle, vrep.simx_opmode_blocking)
if result1 != vrep.simx_return_ok:
    raise Exception('could not get first joint variable')
# print('current value of first joint variable on UR3: theta = {:f}'.format(theta1))

result2, theta2 = vrep.simxGetJointPosition(clientID, joint_two_handle, vrep.simx_opmode_blocking)
if result2 != vrep.simx_return_ok:
    raise Exception('could not get second joint variable')
# print('current value of second joint variable on UR3: theta = {:f}'.format(theta2))

result3, theta3 = vrep.simxGetJointPosition(clientID, joint_three_handle, vrep.simx_opmode_blocking)
if result3 != vrep.simx_return_ok:
    raise Exception('could not get third joint variable')
# print('current value of third joint variable on UR3: theta = {:f}'.format(theta3))

result4, theta4 = vrep.simxGetJointPosition(clientID, joint_four_handle, vrep.simx_opmode_blocking)
if result4 != vrep.simx_return_ok:
    raise Exception('could not get fourth joint variable')
# print('current value of fourth joint variable on UR3: theta = {:f}'.format(theta4))

result5, theta5 = vrep.simxGetJointPosition(clientID, joint_five_handle, vrep.simx_opmode_blocking)
if result5 != vrep.simx_return_ok:
    raise Exception('could not get fifth joint variable')
# print('current value of fifth joint variable on UR3: theta = {:f}'.format(theta5))

result6, theta6 = vrep.simxGetJointPosition(clientID, joint_six_handle, vrep.simx_opmode_blocking)
if result6 != vrep.simx_return_ok:
    raise Exception('could not get sixth joint variable')
# print('current value of sixth joint variable on UR3: theta = {:f}'.format(theta6))

#Inital thetas from the simulator
initial_thetas = [theta1, theta2, theta3, theta4, theta5, theta6]

# Make a dummy for end_effector_pose
result, end_effector_pose = vrep.simxCreateDummy(clientID,0.1,None,vrep.simx_opmode_oneshot_wait)

############################################
# This is totally not working
# #Change from Dummy11 if more dummies added before
# result, end_effector_handle = vrep.simxGetObjectHandle(clientID, "Dummy11", vrep.simx_opmode_blocking)
# if result != vrep.simx_return_ok:
#     raise Exception('could not get object handle for end_effector_pose')
# result, end_effector_position = vrep.simxGetObjectPosition(clientID,  end_effector_handle, -1, vrep.simx_opmode_blocking)
# if result != vrep.simx_return_ok:
#     raise Exception('Could not get object position for end_effector_pose')
#
# # Draw the dummy
# vrep.simxSetObjectPosition(clientID, end_effector_handle, -1, [1,1,1], vrep.simx_opmode_oneshot_wait)
#
# # vrep.simxSetObjectPosition(clientID, dummy_1, dummy_0, [0,0,-0.1], vrep.simx_opmode_oneshot_wait)
#
# print("Move dummy object at the end effector to the position you want to move to")
#
# while(1):
#     result, pos_check = vrep.simxGetObjectPosition(clientID,  end_effector_handle, -1, vrep.simx_opmode_blocking)
#     if result != vrep.simx_return_ok:
#         raise Exception('Could not get object position for end_effector_pose')
#
#     if(pos_check != end_effector_position):
#         break
##########################################

i = 0
while i < 3:
    if(input("Press any key to start: ") == "quit"):
        break

    goal_pose = user_input()
    print ("Calculating path, hang tight!")

    quaternion = quaternion_from_matrix(goal_pose)
    temp = quaternion[0]
    quaternion[0] = quaternion[1]
    quaternion[1] = quaternion[2]
    quaternion[2] = quaternion[3]
    quaternion[3] = temp

    P_final = goal_pose[0:3,3]

    # Draw the dummy
    vrep.simxSetObjectPosition(clientID, end_effector_pose, -1, P_final, vrep.simx_opmode_oneshot_wait)

    thetas_for_ik = np.array([[theta1], [theta2], [theta3], [theta4], [theta5], [theta6]])
    S_mat = [S1, S2, S3, S4, S5, S6]

    # thetas_from_ik = findIK(goal_pose, S, M, thetas_for_ik)
    k = 1

    # T_1in0 = getT_1in0(M, S, thetas_for_ik)
    T_1in0 = evalT(S, thetas_for_ik, M)
    screwMat = logm(np.dot(goal_pose, inv(T_1in0)))
    twist = inv_bracket(screwMat)

    counter = 0
    final_pose_flag = 1

    while (np.linalg.norm(twist) > 0.01):

        if(counter >= 100):
            print("Desired Pose not reachable")
            final_pose_flag = 0
            break

        J = evalJ(S, thetas_for_ik)
        thetas_for_ikdot = np.dot(inv(np.dot(np.transpose(J), J) + 0.1*np.identity(6)),
        np.dot(np.transpose(J), twist)) - np.dot(np.identity(6) - np.dot(np.linalg.pinv(J), J), thetas_for_ik)
        thetas_for_ik = thetas_for_ik + (thetas_for_ikdot * k)

        T_1in0 = evalT(S, thetas_for_ik, M)
        screwMat = logm(np.dot(goal_pose, inv(T_1in0)))
        twist = inv_bracket(screwMat)

    if(final_pose_flag == 0):
        continue


    # thetas_from_ik = thetas_from_ik[0]
    thetas_from_ik = thetas_for_ik

    theta_start = np.array([[initial_thetas[0]], [initial_thetas[1]], [initial_thetas[2]], [initial_thetas[3]], [initial_thetas[4]], [initial_thetas[5]]])
    theta_goal = np.array([[thetas_from_ik[0][0]], [thetas_from_ik[1][0]], [thetas_from_ik[2][0]], [thetas_from_ik[3][0]], [thetas_from_ik[4][0]], [thetas_from_ik[5][0]]])
    step_size = 0.01

    start_pos = np.array([[0],[0],[0]])
    end_pos = np.array([[M[0][3]],[M[1][3]],[M[2][3]]])
    p_robot = np.block([[start_pos, q1, q2, q3, q4, q5, q6, end_pos]])

    r_robot = np.array([[0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03]])
    # r_robot = np.array([[0.12 , 0.18, 0.18, 0.18, 0.18, 0.18, 0.18,0.18 ]])
    # p_obstacle = np.array([[1], [1], [1]])
    r_obstacle = np.array([[0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1]])
    # print(p_obstacle)

    start_root = Node()
    start_root.theta = theta_start
    end_root = Node()
    end_root.theta = theta_goal
    start_curr = start_root
    end_curr = end_root

    counter = 0
    bound = 10

    while counter < bound:
        '''
        Joint Bounds from Lab 3 Doc in degrees:
        theta1 = (60,315)
        theta2 = (-185,5)
        theta3 = (-10,150)
        theta4 = (-190,-80)
        theta5 = (-120,80)
        theta6 = (-180,180)
        '''
        # rand1 = random.uniform(deg2rad(60),deg2rad(315))
        # rand2 = random.uniform(deg2rad(-185),deg2rad(5))
        # rand3 = random.uniform(deg2rad(-10),deg2rad(150))
        # rand4 = random.uniform(deg2rad(-190),deg2rad(-80))
        # rand5 = random.uniform(deg2rad(-120),deg2rad(80))
        # rand6 = random.uniform(deg2rad(-180),deg2rad(180))
        rand1 = random.uniform(-np.pi,np.pi)
        rand2 = random.uniform(-np.pi,np.pi)
        rand3 = random.uniform(-np.pi,np.pi)
        rand4 = random.uniform(-np.pi,np.pi)
        rand5 = random.uniform(-np.pi,np.pi)
        rand6 = random.uniform(-np.pi,np.pi)


        theta_target = np.array([[rand1], [rand2], [rand3], [rand4], [rand5], [rand6]])

        theta_a = start_curr.theta
        theta_b = theta_target

        if(point2point_collision_detector(theta_a, theta_b, S, p_robot, r_robot, p_obstacle, r_obstacle, step_size) is 0):
            start_curr.next = Node()
            start_curr = start_curr.next
            start_curr.theta = theta_target

        theta_a = end_root.theta
        theta_b = theta_target

        if(point2point_collision_detector(theta_a, theta_b, S, p_robot, r_robot, p_obstacle, r_obstacle, step_size) is 0):
            temp = Node()
            temp.next = end_root
            temp.theta = theta_target
            end_root = temp

        q = get_complete_path(start_root, end_root)

        if(q is not None):
            break

        counter += 1

    if counter == bound:
        print("NO PATH POSSIBLE")
        # print("Start list:")
        # print_linked_list(start_root)
        # print("Goal list")
        # print_linked_list(end_root)
    else:
        print("Found path")

        for theta in q:
            # Set the desired value of the first joint variable
            vrep.simxSetJointTargetPosition(clientID, joint_one_handle, theta[0][0], vrep.simx_opmode_oneshot)
            time.sleep(0.1)
            vrep.simxSetJointTargetPosition(clientID, joint_two_handle, theta[1][0], vrep.simx_opmode_oneshot)
            time.sleep(0.1)
            vrep.simxSetJointTargetPosition(clientID, joint_three_handle, theta[2][0], vrep.simx_opmode_oneshot)
            time.sleep(0.1)
            vrep.simxSetJointTargetPosition(clientID, joint_four_handle, theta[3][0], vrep.simx_opmode_oneshot)
            time.sleep(0.1)
            vrep.simxSetJointTargetPosition(clientID, joint_five_handle, theta[4][0], vrep.simx_opmode_oneshot)
            time.sleep(0.1)
            vrep.simxSetJointTargetPosition(clientID, joint_six_handle, theta[5][0], vrep.simx_opmode_oneshot)
            time.sleep(1)

        q.reverse()

        for theta in q:
            # Set the desired value of the first joint variable
            vrep.simxSetJointTargetPosition(clientID, joint_one_handle, theta[0][0], vrep.simx_opmode_oneshot)
            time.sleep(0.1)
            vrep.simxSetJointTargetPosition(clientID, joint_two_handle, theta[1][0], vrep.simx_opmode_oneshot)
            time.sleep(0.1)
            vrep.simxSetJointTargetPosition(clientID, joint_three_handle, theta[2][0], vrep.simx_opmode_oneshot)
            time.sleep(0.1)
            vrep.simxSetJointTargetPosition(clientID, joint_four_handle, theta[3][0], vrep.simx_opmode_oneshot)
            time.sleep(0.1)
            vrep.simxSetJointTargetPosition(clientID, joint_five_handle, theta[4][0], vrep.simx_opmode_oneshot)
            time.sleep(0.1)
            vrep.simxSetJointTargetPosition(clientID, joint_six_handle, theta[5][0], vrep.simx_opmode_oneshot)
            time.sleep(1)

        i += 1

# Remove end_effector_pose dummy
vrep.simxRemoveObject(clientID, end_effector_pose, vrep.simx_opmode_oneshot_wait)

# Remove dummy handles for ConcretBlock1
vrep.simxRemoveObject(clientID, dummy_0, vrep.simx_opmode_oneshot_wait)
vrep.simxRemoveObject(clientID, dummy_1, vrep.simx_opmode_oneshot_wait)
vrep.simxRemoveObject(clientID, dummy_2, vrep.simx_opmode_oneshot_wait)
vrep.simxRemoveObject(clientID, dummy_3, vrep.simx_opmode_oneshot_wait)
vrep.simxRemoveObject(clientID, dummy_4, vrep.simx_opmode_oneshot_wait)
vrep.simxRemoveObject(clientID, dummy_5, vrep.simx_opmode_oneshot_wait)

# Remove dummy handles for ConcretBlock2
vrep.simxRemoveObject(clientID, dummy_6, vrep.simx_opmode_oneshot_wait)
vrep.simxRemoveObject(clientID, dummy_7, vrep.simx_opmode_oneshot_wait)
vrep.simxRemoveObject(clientID, dummy_8, vrep.simx_opmode_oneshot_wait)
vrep.simxRemoveObject(clientID, dummy_9, vrep.simx_opmode_oneshot_wait)
vrep.simxRemoveObject(clientID, dummy_10, vrep.simx_opmode_oneshot_wait)
vrep.simxRemoveObject(clientID, dummy_11, vrep.simx_opmode_oneshot_wait)


# Stop simulation
vrep.simxStopSimulation(clientID, vrep.simx_opmode_oneshot)

# Before closing the connection to V-REP, make sure that the last command sent out had time to arrive. You can guarantee this with (for example):
vrep.simxGetPingTime(clientID)

# Close the connection to V-REP
vrep.simxFinish(clientID)
