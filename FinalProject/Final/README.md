## Final Checkpoint (due 5/9/18)
The final checkpoint code can be seen [here](https://gitlab.engr.illinois.edu/rmaksi2/ECE470/blob/master/FinalProject/Final/final.py).  

### Goal
Our goal for the project is to be able to move a set of two blocks from separate  
initial locations to final location, while avoiding objects in its path. In the  
final position, the blocks will be stacked. The blocks will be initially placed  
at a predetermined location, but the final location will be entered by the user.  
If the position given is invalid, then the program will prompt the user for a  
different goal position. The conditions that will make the goal position invalid  
is if it is impossible to reach the position without collision to the robot, or  
if the blocks collide with the environment.

### Solution Approach
Since the goal of the project was to move the two blocks from different initial  
positions, we had to grab one of the two blocks first. In order to do that, we  
first had to compute the space screw axes, which is computed by function  
computeS starting on line 159. Then, the base frame M had to be calculated,  
which is computed by function computeM starting on line 135. The base frame M  
is calculated using Matthew's Brett Python library in order to transform the  
orientation from Euler angles. Next, we compute the set of current theta values  
for all the joint in the current position. This is done in function computeThetas  
that starts on line 220. We are now ready to compute the set of theta values  
that will correspond to the final pose that our robot needs to achieve. The  
computation is done using the findIK function on line 261. After all these steps  
we are now able to pick up the first block in order to place it at the position  
specified by the user. After the block is picked up, we use V-REP functions to  
move the joints accordingly.  These steps have to be repeated every time we want  
to move the robot to a different position, either for picking up the blocks or  
when the block needs to be moved to some other position. 


