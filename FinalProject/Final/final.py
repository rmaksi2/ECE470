import vrep
import time
import numpy as np
from numpy.linalg import multi_dot, norm, inv
from ece470_lib import *
from usefulFunctions import *
import transforms3d

'''
https://matthew-brett.github.io/transforms3d/reference/transforms3d.euler.html#transforms3d.euler.euler2mat
pip install transforms3d
'''
print (""" \n\n\n\n\n\n\n\n\n\n\n\n""")

print (
"""
Welcome to the final demo for Intro to Robotics class!\n
********** UR3 Robotic Simulation in V-REP ***********\n
******** Introduction to Robotics - ECE 470 **********\n
******************* Spring 2018 **********************\n
***** University of Illinois at Urbana-Champaign *****\n
***** by : Robert Maksimowicz, Pratheek Muskula ******\n
******************** and Siyun He ********************\n
"""
)
print (""" \n\n\n\n\n\n\n\n\n\n\n\n""")

# Close all open connections (just in case)
vrep.simxFinish(-1)

# Connect to V-REP (raise exception on failure)
clientID = vrep.simxStart('127.0.0.1', 19997, True, True, 5000, 5)
if clientID == -1:
    raise Exception('Failed connecting to remote API server')
###############################################################################
time.sleep(5)
print (""" \n\n\n\n\n\n\n\n\n\n\n\n""")
print (
"""
** This video demonstrates the ability of the robot **\n
** to move a set of two blocks from separate initial *\n
***** locations to the final location specified ******\n
******************** by the user. ********************\n
"""
)
print (""" \n\n\n\n\n\n\n\n\n\n\n\n""")
time.sleep(10)
print (""" \n\n\n\n\n\n\n\n\n\n\n\n""")

print (
"""
******* By achieving the goal, it demonstrates *******\n
***********  inverse kinematics in action. ***********\n
"""
)
print (""" \n\n\n\n\n\n\n\n\n\n\n\n""")
time.sleep(5)


################################################################################
time.sleep(1)
vrep.simxSetIntegerSignal(clientID, "BaxterGripper_close", 0, vrep.simx_opmode_oneshot)
# Get the gripper open at the beginning, cause somehow it closes sometimes
################################################################################

# Get the first T_2in0 - for the first block
# Get handle for the Cuboid
result, Cuboid_handle = vrep.simxGetObjectHandle(clientID, 'Cuboid', vrep.simx_opmode_blocking)
if result != vrep.simx_return_ok:
    raise Exception('could not get object handle for the Cuboid')

# Get position of the Cuboid
result, Cuboid_position = vrep.simxGetObjectPosition(clientID, Cuboid_handle, -1, vrep.simx_opmode_blocking)
if result != vrep.simx_return_ok:
    raise Exception('Could not get object position for the Cuboid')
Cuboid_position = np.reshape(Cuboid_position,(3,1)) # Position of the cuboid
# print ("Cuboid position", Cuboid_position)

# Get the orientation from base to  world frame
result, Cuboid_orientation = vrep.simxGetObjectOrientation(clientID, Cuboid_handle, -1, vrep.simx_opmode_blocking)
if result != vrep.simx_return_ok:
    raise Exception('could not get object orientation angles for UR3')

# Orientation of the cuboid
R_cuboid = transforms3d.euler.euler2mat(Cuboid_orientation[0], Cuboid_orientation[1], Cuboid_orientation[2])
# print ("R_cuboid", R_cuboid)


T_2in0 = np.block([
[R_cuboid[0,:], Cuboid_position[0,:]],
[R_cuboid[1,:], Cuboid_position[1,:]],
[R_cuboid[2,:], Cuboid_position[2,:]],
[0,0,0,1] ])
# print ("T_2in0", T_2in0)

################################################################################

# Get joint object handles
result, joint_one_handle = vrep.simxGetObjectHandle(clientID, 'UR3_joint1', vrep.simx_opmode_blocking)
if result != vrep.simx_return_ok:
    raise Exception('could not get object handle for first joint')

result, joint_two_handle = vrep.simxGetObjectHandle(clientID, 'UR3_joint2', vrep.simx_opmode_blocking)
if result != vrep.simx_return_ok:
    raise Exception('could not get object handle for second joint')

result, joint_three_handle = vrep.simxGetObjectHandle(clientID, 'UR3_joint3', vrep.simx_opmode_blocking)
if result != vrep.simx_return_ok:
    raise Exception('could not get object handle for third joint')

result, joint_four_handle = vrep.simxGetObjectHandle(clientID, 'UR3_joint4', vrep.simx_opmode_blocking)
if result != vrep.simx_return_ok:
    raise Exception('could not get object handle for fourth joint')

result, joint_five_handle = vrep.simxGetObjectHandle(clientID, 'UR3_joint5', vrep.simx_opmode_blocking)
if result != vrep.simx_return_ok:
    raise Exception('could not get object handle for fifth joint')

result, joint_six_handle = vrep.simxGetObjectHandle(clientID, 'UR3_joint6', vrep.simx_opmode_blocking)
if result != vrep.simx_return_ok:
    raise Exception('could not get object handle for sixth joint')

#Get handel for UR3
result, UR3_handle = vrep.simxGetObjectHandle(clientID, 'UR3', vrep.simx_opmode_blocking)
if result != vrep.simx_return_ok:
    raise Exception('could not get object handle for UR3')
#Get handel for UR3_connection
result, UR3_connection = vrep.simxGetObjectHandle(clientID, 'UR3_connection', vrep.simx_opmode_blocking)
if result != vrep.simx_return_ok:
    raise Exception('could not get object handle for UR3UR3_connection')
################################################################################
#Start simulation
vrep.simxStartSimulation(clientID, vrep.simx_opmode_oneshot)
################################################################################
def computeM():
    # Get the orientation from base to  world frame
    result, orientation = vrep.simxGetObjectOrientation(clientID, UR3_connection, UR3_handle, vrep.simx_opmode_blocking)
    if result != vrep.simx_return_ok:
        raise Exception('could not get object orientation angles for UR3')

    # Get the position from base to world frame
    result, p = vrep.simxGetObjectPosition(clientID, UR3_connection, UR3_handle, vrep.simx_opmode_blocking)
    if result != vrep.simx_return_ok:
        raise Exception('could not get object current position for UR3')
    P_initial = np.reshape(p,(3,1))
    # print ("P_initial", P_initial)
    R_initial = transforms3d.euler.euler2mat(orientation[0], orientation[1], orientation[2])
    # print ("R_itinial", R_initial)

    M = np.block([
    [R_initial[0,:], P_initial[0,:]],
    [R_initial[1,:], P_initial[1,:]],
    [R_initial[2,:], P_initial[2,:]],
    [0,0,0,1] ])
    # print ("M", M, "\n")
    return M
################################################################################

def computeS():
    # Set up scew axis with respect to base frame
    result, q1 = vrep.simxGetObjectPosition(clientID, joint_one_handle, UR3_handle, vrep.simx_opmode_blocking)
    if result != vrep.simx_return_ok:
        raise Exception('could not get object current position for UR3')
    q1 = np.reshape(q1,(3,1))
    a1 = np.array([[0],[0],[1]])
    S1 = toScrew(a1, q1)
    # print ("q1", q1)
    # print ("S1", S1)

    result, q2 = vrep.simxGetObjectPosition(clientID, joint_two_handle, UR3_handle, vrep.simx_opmode_blocking)
    if result != vrep.simx_return_ok:
        raise Exception('could not get object current position for UR3')
    q2 = np.reshape(q2,(3,1))
    a2 = np.array([[-1],[0],[0]])
    S2 = toScrew(a2, q2)
    # print ("q2", q2)
    # print ("S2", S2)

    result, q3 = vrep.simxGetObjectPosition(clientID, joint_three_handle, UR3_handle, vrep.simx_opmode_blocking)
    if result != vrep.simx_return_ok:
        raise Exception('could not get object current position for UR3')
    q3 = np.reshape(q3,(3,1))
    a3 = np.array([[-1],[0],[0]])
    S3 = toScrew(a3, q3)
    # print ("q3", q3)
    # print ("S3", S3)

    result, q4 = vrep.simxGetObjectPosition(clientID, joint_four_handle, UR3_handle, vrep.simx_opmode_blocking)
    if result != vrep.simx_return_ok:
        raise Exception('could not get object current position for UR3')
    q4 = np.reshape(q4,(3,1))
    a4 = np.array([[-1],[0],[0]])
    S4 = toScrew(a4, q4)
    # print ("q4", q4)
    # print ("S4", S4)

    result, q5 = vrep.simxGetObjectPosition(clientID, joint_five_handle, UR3_handle, vrep.simx_opmode_blocking)
    if result != vrep.simx_return_ok:
        raise Exception('could not get object current position for UR3')
    q5 = np.reshape(q5,(3,1))
    a5 = np.array([[0],[0],[1]])
    S5 = toScrew(a5, q5)
    # print ("q5", q5)
    # print ("S5", S5)

    result, q6 = vrep.simxGetObjectPosition(clientID, joint_six_handle, UR3_handle, vrep.simx_opmode_blocking)
    if result != vrep.simx_return_ok:
        raise Exception('could not get object current position for UR3')
    q6 = np.reshape(q6,(3,1))
    a6 = np.array([[-1],[0],[0]])
    S6 = toScrew(a6, q6)
    # print ("q6", q6)
    # print ("S6", S6)

    S = np.block([[S1, S2, S3, S4, S5, S6]])
    # print ("S", S)
    return S

############### Get the theta values for joint variables #############################
def computeThetas():
    result1, theta1 = vrep.simxGetJointPosition(clientID, joint_one_handle, vrep.simx_opmode_blocking)
    if result1 != vrep.simx_return_ok:
        raise Exception('could not get first joint variable')
    # print('current value of first joint variable on UR3: theta = {:f}'.format(theta1))

    result2, theta2 = vrep.simxGetJointPosition(clientID, joint_two_handle, vrep.simx_opmode_blocking)
    if result2 != vrep.simx_return_ok:
        raise Exception('could not get second joint variable')
    # print('current value of second joint variable on UR3: theta = {:f}'.format(theta2))

    result3, theta3 = vrep.simxGetJointPosition(clientID, joint_three_handle, vrep.simx_opmode_blocking)
    if result3 != vrep.simx_return_ok:
        raise Exception('could not get third joint variable')
    # print('current value of third joint variable on UR3: theta = {:f}'.format(theta3))

    result4, theta4 = vrep.simxGetJointPosition(clientID, joint_four_handle, vrep.simx_opmode_blocking)
    if result4 != vrep.simx_return_ok:
        raise Exception('could not get fourth joint variable')
    # print('current value of fourth joint variable on UR3: theta = {:f}'.format(theta4))

    result5, theta5 = vrep.simxGetJointPosition(clientID, joint_five_handle, vrep.simx_opmode_blocking)
    if result5 != vrep.simx_return_ok:
        raise Exception('could not get fifth joint variable')
    # print('current value of fifth joint variable on UR3: theta = {:f}'.format(theta5))

    result6, theta6 = vrep.simxGetJointPosition(clientID, joint_six_handle, vrep.simx_opmode_blocking)
    if result6 != vrep.simx_return_ok:
        raise Exception('could not get sixth joint variable')
    # print('current value of sixth joint variable on UR3: theta = {:f}'.format(theta6))

    #Inital thetas from the simulator
    theta = np.array([[theta1], [theta2], [theta3], [theta4], [theta5], [theta6]])
    # theta = np.array([[0.1], [0.1], [0.1], [0.1], [0.1], [0.1]])
    # print (theta)
    return theta
################################################################################
# Let's use inverse kinematics to move the robot joints to grab the block
S = computeS()
M = computeM()
theta = computeThetas()
theta, V = findIK(T_2in0, S, M, theta)
time.sleep(2)

# T_2in0 is the goal
# Get position and orientation of T_2in0 to visualize the dummy if needed
P_final = T_2in0[0:3,3]

# Predifined theta's to grab the block
print("Let's get the first block!")

# vrep.simxSetObjectPosition(clientID, dummy_handle_0, -1, P_final, vrep.simx_opmode_oneshot_wait)
# Set the desired desired positions of joints based on the set of theta's
vrep.simxSetJointTargetPosition(clientID, joint_one_handle, theta[0], vrep.simx_opmode_streaming)
# time.sleep(1)
vrep.simxSetJointTargetPosition(clientID, joint_five_handle, theta[4], vrep.simx_opmode_streaming)
# time.sleep(1)
vrep.simxSetJointTargetPosition(clientID, joint_six_handle, theta[5], vrep.simx_opmode_streaming)
# time.sleep(1)
vrep.simxSetJointTargetPosition(clientID, joint_three_handle, theta[2], vrep.simx_opmode_streaming)
time.sleep(1)
vrep.simxSetJointTargetPosition(clientID, joint_four_handle, theta[3], vrep.simx_opmode_streaming)
time.sleep(1)
vrep.simxSetJointTargetPosition(clientID, joint_two_handle, theta[1], vrep.simx_opmode_streaming)

# Close the gripper
time.sleep(1)
vrep.simxSetIntegerSignal(clientID, "BaxterGripper_close", 1, vrep.simx_opmode_oneshot)
# Gripper should close now
time.sleep(2)

################################################################################
# Move the robot away from the block
# print("Straightning up")
vrep.simxSetJointTargetPosition(clientID, joint_two_handle, 0, vrep.simx_opmode_streaming)
time.sleep(1)
# vrep.simxSetJointTargetPosition(clientID, joint_one_handle, 0, vrep.simx_opmode_streaming)
# time.sleep(1)
vrep.simxSetJointTargetPosition(clientID, joint_three_handle, 0, vrep.simx_opmode_streaming)
time.sleep(1)


################################################################################
# Set the block on the user specified position
S = computeS()
M = computeM()
T_2in0 = user_input()
theta, V = findIK(T_2in0, S, M, theta)
vrep.simxSetJointTargetPosition(clientID, joint_one_handle, theta[0], vrep.simx_opmode_streaming)
# time.sleep(1)
vrep.simxSetJointTargetPosition(clientID, joint_five_handle, theta[4], vrep.simx_opmode_streaming)
# time.sleep(1)
vrep.simxSetJointTargetPosition(clientID, joint_six_handle, theta[5], vrep.simx_opmode_streaming)
# time.sleep(1)
vrep.simxSetJointTargetPosition(clientID, joint_three_handle, theta[2], vrep.simx_opmode_streaming)
time.sleep(1)
vrep.simxSetJointTargetPosition(clientID, joint_four_handle, theta[3], vrep.simx_opmode_streaming)
time.sleep(1)
vrep.simxSetJointTargetPosition(clientID, joint_two_handle, theta[1], vrep.simx_opmode_streaming)
# Wait two seconds
time.sleep(1)
print ("Releasing the block")
# Release the block
vrep.simxSetIntegerSignal(clientID, "BaxterGripper_close", 0, vrep.simx_opmode_oneshot)
# Gripper should open now
time.sleep(3)

################################################################################
# Get the robot to initial position
print("Getting back to initial position")
theta = np.array([ [0],[0],[0],[0],[0],[0] ])
vrep.simxSetJointTargetPosition(clientID, joint_two_handle, theta[1], vrep.simx_opmode_streaming)
time.sleep(1)
vrep.simxSetJointTargetPosition(clientID, joint_one_handle, theta[0], vrep.simx_opmode_streaming)
vrep.simxSetJointTargetPosition(clientID, joint_three_handle, theta[2], vrep.simx_opmode_streaming)
vrep.simxSetJointTargetPosition(clientID, joint_four_handle, theta[3], vrep.simx_opmode_streaming)
vrep.simxSetJointTargetPosition(clientID, joint_five_handle, theta[4], vrep.simx_opmode_streaming)
vrep.simxSetJointTargetPosition(clientID, joint_six_handle, theta[5], vrep.simx_opmode_streaming)
time.sleep(2)
################################################################################
# Pick up the second block
# Let's use inverse kinematics to move the robot joints to grab the block
S = computeS()
M = computeM()
theta = computeThetas()
theta, V = findIK(T_2in0, S, M, theta)

# T_2in0 is the goal
# Get position and orientation of T_2in0 to visualize the dummy if needed
P_final = T_2in0[0:3,3]

# Predifined theta's to grab the block
print("Let's get the second block!")

# Set the desired desired positions of joints based on the set of theta's
vrep.simxSetJointTargetPosition(clientID, joint_one_handle, theta[0], vrep.simx_opmode_streaming)
# time.sleep(1)
vrep.simxSetJointTargetPosition(clientID, joint_five_handle, theta[4], vrep.simx_opmode_streaming)
# time.sleep(1)
vrep.simxSetJointTargetPosition(clientID, joint_six_handle, theta[5], vrep.simx_opmode_streaming)
# time.sleep(1)
vrep.simxSetJointTargetPosition(clientID, joint_three_handle, theta[2], vrep.simx_opmode_streaming)
time.sleep(1)
vrep.simxSetJointTargetPosition(clientID, joint_four_handle, theta[3], vrep.simx_opmode_streaming)
time.sleep(1)
vrep.simxSetJointTargetPosition(clientID, joint_two_handle, theta[1], vrep.simx_opmode_streaming)

# Close the gripper
time.sleep(1)
vrep.simxSetIntegerSignal(clientID, "BaxterGripper_close", 1, vrep.simx_opmode_oneshot)
# Gripper should close now
time.sleep(3)
################################################################################
# Move the robot away from the block
vrep.simxSetJointTargetPosition(clientID, joint_two_handle, 0, vrep.simx_opmode_streaming)
time.sleep(1)
vrep.simxSetJointTargetPosition(clientID, joint_three_handle, 0, vrep.simx_opmode_streaming)
time.sleep(1)
################################################################################
# Set the block on the user specified position
S = computeS()
M = computeM()
print("Stack the second block on top of the first block")
theta, V = findIK(T_2in0, S, M, theta)

vrep.simxSetJointTargetPosition(clientID, joint_one_handle, theta[0], vrep.simx_opmode_streaming)
# time.sleep(1)
vrep.simxSetJointTargetPosition(clientID, joint_five_handle, theta[4], vrep.simx_opmode_streaming)
# time.sleep(1)
vrep.simxSetJointTargetPosition(clientID, joint_six_handle, theta[5], vrep.simx_opmode_streaming)
vrep.simxSetJointTargetPosition(clientID, joint_three_handle, theta[2], vrep.simx_opmode_streaming)
time.sleep(1)
vrep.simxSetJointTargetPosition(clientID, joint_four_handle, theta[3], vrep.simx_opmode_streaming)
time.sleep(2)
temp = theta[1]
theta[1] = temp * 0.7
# print("1")
vrep.simxSetJointTargetPosition(clientID, joint_two_handle, theta[1], vrep.simx_opmode_streaming)
time.sleep(2)
# print("2")
theta[1] = temp * 0.8
vrep.simxSetJointTargetPosition(clientID, joint_two_handle, theta[1], vrep.simx_opmode_streaming)
time.sleep(2)
# print("3")
theta[1] = temp * 0.9
vrep.simxSetJointTargetPosition(clientID, joint_two_handle, theta[1], vrep.simx_opmode_streaming)
time.sleep(2)
# print("4")
theta[1] = temp
vrep.simxSetJointTargetPosition(clientID, joint_two_handle, theta[1], vrep.simx_opmode_streaming)
time.sleep(1)
print ("Releasing the block")
# Release the block
vrep.simxSetIntegerSignal(clientID, "BaxterGripper_close", 0, vrep.simx_opmode_oneshot)
# Gripper should open now
time.sleep(3)
################################################################################
# Get the robot to initial position
print("Getting back to initial position")
theta = np.array([ [0],[0],[0],[0],[0],[0] ])
vrep.simxSetJointTargetPosition(clientID, joint_two_handle, theta[1], vrep.simx_opmode_streaming)
time.sleep(1)
vrep.simxSetJointTargetPosition(clientID, joint_one_handle, theta[0], vrep.simx_opmode_streaming)
vrep.simxSetJointTargetPosition(clientID, joint_three_handle, theta[2], vrep.simx_opmode_streaming)
vrep.simxSetJointTargetPosition(clientID, joint_four_handle, theta[3], vrep.simx_opmode_streaming)
vrep.simxSetJointTargetPosition(clientID, joint_five_handle, theta[4], vrep.simx_opmode_streaming)
vrep.simxSetJointTargetPosition(clientID, joint_six_handle, theta[5], vrep.simx_opmode_streaming)
time.sleep(2)
################################################################################
print (""" \n\n\n\n\n\n\n\n\n\n\n\n""")
print (
"""
** The robot succesfully moved the set of blocks to **\n
********* the position specified by the user *********\n
"""
)
print (""" \n\n\n\n\n\n\n\n\n\n\n\n""")
time.sleep(5)
print (""" \n\n\n\n\n\n\n\n\n\n\n\n""")
print (
"""
********** UR3 Robotic Simulation in V-REP ***********\n
******** Introduction to Robotics - ECE 470 **********\n
******************* Spring 2018 **********************\n
***** University of Illinois at Urbana-Champaign *****\n
***** by : Robert Maksimowicz, Pratheek Muskula ******\n
******************** and Siyun He *********************\n
"""
)
print (""" \n\n\n\n\n\n\n\n\n\n\n\n""")
time.sleep(5)

# Stop simulation
vrep.simxStopSimulation(clientID, vrep.simx_opmode_oneshot)

# Before closing the connection to V-REP, make sure that the last command sent out had time to arrive. You can guarantee this with (for example):
vrep.simxGetPingTime(clientID)

# Close the connection to V-REP
vrep.simxFinish(clientID)
